package ru.edu.project.notify.service;

import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import ru.edu.project.backend.api.notification.NotifyMessage;

import javax.mail.internet.MimeMessage;

@Service
public class EmailService {

    /**
     * Логгер.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    /**
     * Зависимость на отправку email.
     */
    @Autowired
    private JavaMailSender mailSender;

    /**
     * Зависимость на шаблонизатор.
     */
    @Autowired
    private SpringTemplateEngine templateEngine;

    /**
     * Переключатель для отключения отправки реальных писем.
     */
    @Value("${DISABLE_REAL_EMAIL:false}")
    private boolean disableRealSendingEmail;


    /**
     * Отправка сообщения клиенту.
     *
     * @param notifyMessage
     */
    public void process(final NotifyMessage notifyMessage) {
        Context ctx = new Context();
        ctx.setVariables(notifyMessage.getModel());

        String renderedString = templateEngine.process(notifyMessage.getTemplateId(), ctx);

        sendEmail(notifyMessage, renderedString);

    }

    @SneakyThrows
    private void sendEmail(final NotifyMessage message, final String body) {

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        helper.setFrom("Демонстрационный проект <java_course@shariktlt.ru>");
        helper.setTo(message.getEmail());
        helper.setSubject(message.getSubject());
        helper.setText(body, true);

        LOGGER.info("{} письма {}:\n{}", disableRealSendingEmail ? "Отладка" : "Отправка", message.getEmail(), body);
        if (!disableRealSendingEmail) {
            mailSender.send(mimeMessage);
        }
    }
}
