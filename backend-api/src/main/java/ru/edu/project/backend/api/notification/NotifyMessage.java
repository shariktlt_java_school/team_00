package ru.edu.project.backend.api.notification;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.util.Map;

@Getter
@Setter
@Builder
@Jacksonized
public class NotifyMessage {

    /**
     * Email.
     */
    private String email;

    /**
     * Subject.
     */
    private String subject;

    /**
     * TemplateId.
     */
    private String templateId;

    /**
     * Model.
     */
    private Map<String, Object> model;
}
