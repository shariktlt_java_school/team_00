package ru.edu.project.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.edu.project.backend.api.user.UserInfo;
import ru.edu.project.backend.api.user.UserService;
import ru.edu.project.backend.da.UserDALayer;

import static java.util.Optional.ofNullable;

@Service
public class UserServiceLayer implements UserService {

    /**
     * Пустой пользователь.
     */
    public static final UserInfo EMPTY_USER = UserInfo.builder().id(-1L).build();

    /**
     * Зависимость сразу на DA слой. Логику тут не применяем.
     */
    @Autowired
    private UserDALayer userServiceDA;


    /**
     * Зависимость на сервис уведомлений.
     * Циклическая зависимость.
     */
    @Autowired
    @Lazy
    private NotifyServiceLayer notifyServiceLayer;


    /**
     * Регистрация пользователя.
     *
     * @param userInfo
     * @return
     */
    @Override
    public Long register(final UserInfo userInfo) {
        UserInfo user = userServiceDA.register(userInfo);
        if (user == null) {
            throw new IllegalStateException("не удалось зарегистрироваться");
        }
        return user.getId();
    }

    /**
     * Получение данных о пользователе.
     *
     * @param username
     * @return
     */
    @Override
    public UserInfo loadUserByUsername(final String username) {
        return ofNullable(userServiceDA.findByUsername(username)).orElse(EMPTY_USER);
    }

    /**
     * Обновление данных о пользователе.
     *
     * @param userInfo
     * @return updatedUserInfo
     */
    @Override
    public UserInfo update(final UserInfo userInfo) {
        UserInfo saved = userServiceDA.findById(userInfo.getId());
        if (saved == null) {
            throw new IllegalArgumentException("user not found");
        }

        saved.setPassword(userInfo.getPassword());
        saved.setEmail(userInfo.getEmail());
        saved.setEmailCode(userInfo.getEmailCode());
        saved.setEmailConfirmed(userInfo.getEmailConfirmed());

        return userServiceDA.update(saved);
    }

    /**
     * Поиск по userId.
     *
     * @param id
     * @return userInfo
     */
    @Override
    public UserInfo findById(final Long id) {
        return userServiceDA.findById(id);
    }

    /**
     * Обновление email.
     *
     * @param id
     * @param email
     * @return true
     */
    @Override
    public boolean updateEmail(final Long id, final String email) {
        UserInfo user = findById(id);
        if (user == null) {
            throw new IllegalArgumentException("user id invalid");
        }

        if (email != null && !email.isEmpty() && !email.equals(user.getEmail())) {
            user.setEmail(email);
            user.setEmailConfirmed(false);
            userServiceDA.update(user);
            notifyServiceLayer.sendConfirmationCode(id);
            return true;
        }

        return false;
    }
}
